# LocTalk

simple utility for sending messages on a local network

## About

this utility uses the [AwesomeNet package](https://gitlab.com/TzintzeneT/awesomenet) for Java, and demonstrating one of the usecases for it.

## License
GNU GPLv3 (my favorit)

## Created using FOSS
Because FOSS is awesome
