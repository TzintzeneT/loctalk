import AwesomeNet.*;

import java.util.Scanner;

public class loctalk {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("cannot open a chat without a port");
            return;
        }

        int port = Integer.parseInt(args[0]);
        based user;

        try {
            user = new AwesomeServer(port);
            ((AwesomeServer) user).startServer();
        } catch (Exception e) {
            user = new SomeClient(port);
            ((SomeClient) user).startClient();
        }

        based finalUser = user;

        Thread listenThread = new Thread() {
            public void run() {
                finalUser.listen();
                System.exit(0);
            }
        };
        listenThread.start();

        while (true) {
            user.sendMessage(in.nextLine());
        }
    }
}
